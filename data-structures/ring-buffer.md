# Ring Buffers

FIFO queue with a bounded max capacity


## Technically

- One contiguous region of memory  (size = capacity * slot size)

- Entries are written to/read from slots
  - Can be the actual entries or references to entries
  - Writing to a slot occupies it
  - Reading from a slot make it unoccupied

- Contiguous fixed size region of memory
  - Entries are written to the next unoccupied slot
  - When the end of the buffer is reached then next slot is the first slot in the buffer

- Advantages:
  - Memory stability ( One fixed memory allocation )


## Concerns

- Overflow
  - What do you do when the buffer is full
  - Overwrite, wait (if multithreaded), drop

- Wrap
  - When the last slot in the memory region has been used start at the first
  - Ensure you identify this correctly

- Identification of slot state (occupied/unoccupied)
  - Approach used needs to be consistent across both read and write operations

- Identification of available capacity (empty/full)
  - Needs some way of identifying how full the buffer is
  - Needs to take account of the wrap around nature of the buffer


## Implemenations

- Modular approach
- Track occupied slots approach


### Modular approach

- write_index && read_index start at 0
- guards:
  - empty: read_index == write_index
  - full: write_index + 1 % capacity == read_index // the next slot after the one to write to is the start of the buffer


- __write__
  - guard: not full
  - buffer[write_index] = value
  - write_index = (write_index + 1) % capacity


- __read__
  - guard: not empty
  - value = buffer[read_index]
  - read_index = (read_index + 1) % capacity;


- How the concerns are addressed:
  - Overflow:
    - drop
    - enforced by the guard clause in the write

  - Wrap
    - modular operation for calculating the next slot
    - slot addressing is 0..capacity-1

  - Identification of available capacity (empty/full)
    - :Empty
      - read concern
      - write_index and read_index both point to the same spot

    - :Full
      - Guard clause in the write operation
      - note. means there is always one slot in the buffer that can not be written to
        - the guard checks that the next slot is not the current read slot
        - writes are done to the current write_index slot
        - write slots are considered unoccupied
        - so: the write slot imediately prior to the read slot will always remain unoccupied.
        - why: without it you can not distinguish between empty and full using the empty identification apprach.


### Occupied slots approach

- write_index, read_index, && occupied_slots = 0
- guards:
  - empty: occupied_slots == 0
  - full: occupied_slots != capacity

- __write__:
  - guard: not full
  - buffer[write_index] = value
  - write_index = (write_index +1) % capacity
  - occupied_slots++

- __read__:
  - guard: not empty
  - value = buffer[read_index]
  - read_index = (write_index +1) % capacity
  - occupied_slots--

- How the concerns are addressed:
  - Overflow:
    - drop
    - enforced by the guard clause in the write

  - Wrap
    - modular operation for calculating the next slot
    - slot addressing is 0..capacity-1

  - Identification of available capacity (full/empty)
    - Empty:
      - read concern
      - check there are occupied slots

    - Full:
      - write concern
      - check number of occupied slots is not equal to the capacity
      - note.
        - This removes the need for a sentinel as full/empty are not identified by buffer position
        - Trade off is one extra opetration per read/write that required additional memeory access
        - The hope is cache locality will nullify thaclear

## Glossary

- Slot, a poistion in the buffer that either holds an entry or a pointer an to an entry
