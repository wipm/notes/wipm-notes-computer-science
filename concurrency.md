# Concurrency

- The state of having multiple units of processing in progress at the same time
- Achived via:
  - Simultanious execution of processes at the same time (Multi threaded or Distributed)
  - Times sliced, scheduling each unit of proceessing a period of exectution ( Multiplexing)

- Value
  - Efficient use of resources ( Scalability, minimize idle time )
  - Reduce wait time/latency ( Fairness, for unit of processing to complete )

- Concerns
  - co-ordination to ensure
    - fairness in scheduling, each unit of processing is allowed to make progress
    - Safe access to resouces, prevent data races and resouce starvation

- Data Races
  - Issue where two or more units of processing access data concurrently in such a way that it can lead to corruption
  - Condition:
    - Simultaneous action, multiple units of processing access the same memory loactions
    - At least one write operation
    - Lack of synchronization

- Synchronization
  - Locking, access control to shared data
  - Signaling, co-ordination between processes

- Synchronization Primitives

  - Mutex
    - synchronizastion that guards access
    - single entry
    - use to enforce single access to region of memory.

  - Semaphore
    - synchronization that guards access
    - multiple but bounded entry
    - use to restrcit access to pooled resources

  - Read-Write locks
    - synchronization to guard access
    - multiple concurrent access for read, mutually exclusive write access

  - Spin-locks
    - synchronization that does not yeild the processor (Busy wait)

  - Barrier
    - Synchronization that only allows threads to pass when all participating threads have reached that point

  - Latch
    - Synchronization that allows threads to pass once a condition has occurred

  - Atomic operations
    - Indivisible operations that are guarantied execution without interuption

- Issues
  - Deadlocks
    - Two units of processing waiting for each other to release a lock while holding a lock that each other requires.
    - Conditions:
      - Mutual Exclusion, At least one resource must be held in a non-sharable state
      - Hold & Wait, Hold one resou while simultaneously waiting to aquire additional resources
      - No Preemption, Resources can not be forcibly taken away from a unit of processing
      - Circular Waits, A circular chain of two or more units of processing each of which is waiting for a resource held by the next process in the chain
  - Livelocks
    - Two units of processing try to resolve a deadlock (presumably by releasing locks) but can not make progress because they continually change their status in response to the other
  - Starvation
    - A unit of processing is denied access to the resources it needs
  - Priority Invertion
    - A Lower priority unit of processing holds a lock that prevents higher priority unit of processing from progressing
  - Resource contention, mltiple items competing for a shared resource

